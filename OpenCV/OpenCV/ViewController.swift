//
//  ViewController.swift
//  OpenCV
//
//  Created by Guilherme on 6/27/16.
//  Copyright © 2016 Guilherme. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var openCVVersionLabel: UILabel!
    @IBOutlet weak var yayBoy: UIImageView!
    
    @IBAction func grayScaleTouched(sender: UIButton) {
        yayBoy.image = OpenCVWrapper.makeGrayFromImage(yayBoy.image)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        openCVVersionLabel.text = OpenCVWrapper.openCVVersionString()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

