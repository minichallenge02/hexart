//
//  OpenCVWrapper.m
//  OpenCV
//
//  Created by Guilherme on 6/27/16.
//  Copyright © 2016 Guilherme. All rights reserved.
//

#import "OpenCVWrapper.h"
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>

@implementation OpenCVWrapper

+ (NSString *) openCVVersionString {
    return [NSString stringWithFormat: @"OpenCV Version %s", CV_VERSION];
}

+ (UIImage *) makeGrayFromImage:(UIImage *)image{
    
    // Transformar UIImage para cv::Mat
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    // Se a imagem já estiver em P&B, retorna a mesma imagem
    if(imageMat.channels() == 1){
        return image;
    }
    
    // Transforma a cor da imagem cv::Mat para cinza
    cv::Mat glitchMat;
    glitchMat = imageMat;
    
    for(int col = 0; col < glitchMat.cols; col++){
        for(int row = 0; row < glitchMat.rows; row++){
            glitchMat.row(row).col(col).at<uchar>(0, 0) *= 0.192;
            glitchMat.row(row).col(col).at<uchar>(0, 1) *= 2;
            glitchMat.row(row).col(col).at<uchar>(0, 1) *= 3;
        }
    }
    
    // Transforma a grayMat para UIImage
    return MatToUIImage(glitchMat);
}

@end