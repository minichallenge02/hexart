//
//  OpenCVWrapper.h
//  OpenCV
//
//  Created by Guilherme on 6/27/16.
//  Copyright © 2016 Guilherme. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject

+ (NSString *) openCVVersionString;
+ (UIImage *) makeGrayFromImage: (UIImage *) image;

@end
